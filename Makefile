all: base lilypond build-tools fortran idris

base:
	docker build ./base -t derwanderer99/base

lilypond: base
	docker build ./lilypond -t derwanderer99/lilypond -f lilypond/Dockerfile

build-tools: base
	docker build ./build-tools -t derwanderer99/build-tools -f build-tools/Dockerfile

fortran:
	docker build ./fortran -t derwanderer99/fortran -f fortran/Dockerfile

idris:
	docker build ./idris -t derwanderer99/idris -f idris/Dockerfile

.PHONY: all base lilypond build-tools fortran idris
